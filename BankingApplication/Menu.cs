﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BankingApplication
{
    public partial class Menu : Form
    {
        public Menu()
        {
            InitializeComponent();
        }

        

        private void updateSearchAccountToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UpdationForm up = new UpdationForm();
            up.MdiParent = this;
            up.Show();
            
        }

        private void debitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Deposit dp = new Deposit();
            dp.MdiParent = this;
            dp.Show();
        }

      
        private void allCustomersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AllCustomers C1 = new AllCustomers();
            C1.MdiParent = this;
            C1.Show();
        }

        private void widthdrawToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Widthdraw wdraw = new Widthdraw();
            wdraw.MdiParent = this;
            wdraw.Show();

        }

        private void transferToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Transfer tf = new Transfer();
            tf.MdiParent = this;
            tf.Show();
        }

        private void fDFormToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FdForm fdform = new FdForm();
            fdform.MdiParent = this;
            fdform.Show();
        }

        private void balanceSheetToolStripMenuItem_Click(object sender, EventArgs e)
        {
            BalanceSheet bls = new BalanceSheet();
            bls.MdiParent = this;
            bls.Show();

        }

        private void viewFDToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ViewFD viewfd = new ViewFD();
            viewfd.MdiParent = this;
            viewfd.Show();

        }

        private void logoutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void changePasswordToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ChangePassword cp = new ChangePassword();
            cp.MdiParent = this;
            cp.Show();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void newAccountToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            NewAccount newAcc = new NewAccount();
            newAcc.MdiParent = this;
            newAcc.Show();
        }
    }
}
